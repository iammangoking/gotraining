package slice

import "fmt"

func Slice() {
	var a = [10]int{1, 2, 3, 4, 5}
	var b = a[1:2:5] // b[0] == a[1], b is a[[1], 2, 3, 4]
	b[0]++           // a[1 + 0]
	fmt.Println(a, len(a), cap(a))
	fmt.Println(b, len(b), cap(b))

	b = b[:3] // b[0] == a[1], b is a[[1, 2, 3], 4]
	b[0]++    // a[1 + 0]
	fmt.Println(a, len(a), cap(a))
	fmt.Println(b, len(b), cap(b))

	b = b[2:4] // b[0] == a[1 + 2], b is a[[3, 4]]
	b[0]++     // a[1 + 2 + 0]
	fmt.Println(a, len(a), cap(a))
	fmt.Println(b, len(b), cap(b))
}
