package closure

import (
	"fmt"
	"sync"
)

func Closure() {
	var wg sync.WaitGroup
	//var rw sync.RWMutex
	v := 0
	fmt.Println("v =", v)
	for index := 0; index < 10; index++ {
		wg.Add(1)
		go func() {
			//rw.Lock()
			v++
			//rw.Unlock()
			fmt.Println("v in go =", v)
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println("v =", v)
}
