package httpserver

import (
	"html/template"
	"log"
	"net/http"
)

const templ = `<p>A: {{.A}}</p><p>B: {{.B}}</p>`

func ServerStart() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		//fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
		t := template.Must(template.New("escape").Parse(templ))
		var data struct {
			A string        // untrusted plain text
			B template.HTML // trusted HTML
		}
		data.A = "<b>Hello!</b>"
		data.B = "<b>Hello!</b>"
		if err := t.Execute(w, data); err != nil {
			log.Fatal(err)
		}
	})
	http.ListenAndServe("", nil)
}
