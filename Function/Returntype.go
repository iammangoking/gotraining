package function

import "sort"

type Ret struct {
	Value int
}

func getByRef() *Ret {
	var r Ret
	return &r
	//return new(Ret)
}

func getByVal() Ret {
	var r Ret
	return r
}

func FunctionReturn() {
	getByRef().Value = 10 //getByRef() is lvalue
	//getByVal().Value = 20	// getByVal() is rvalue
	var val = getByVal()
	val.Value = 30 // val is lvalue

	topoSort(prereqs)
}

var prereqs = map[string][]string{
	"algorithms": {"data structures"},
	"calculus":   {"linear algebra"},
	"compilers": {
		"data structures",
		"formal languages",
		"computer organization",
	},
	"data structures":       {"discrete math"},
	"databases":             {"data structures"},
	"discrete math":         {"intro to programming"},
	"formal languages":      {"discrete math"},
	"networks":              {"operating systems"},
	"operating systems":     {"data structures", "computer organization"},
	"programming languages": {"data structures", "computer organization"},
}

func topoSort(m map[string][]string) []string {
	var order []string
	seen := make(map[string]bool)
	var visitAll func(items []string)
	visitAll = func(items []string) {
		        ...
				visitAll(m[item])
				...
	}
		}
	}
	var keys []string
	for key := range m {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	visitAll(keys)
	return order
}
