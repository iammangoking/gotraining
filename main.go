package main

import (
	"GoTraining/Compose"
)

func main() {
	//slice.Slice()
	//closure.Closure()
	//json.Jsontest()
	//httpserver.ServerStart()
	//function.FunctionReturn()
	server := new(Compose.GameServer)
	server.Start()
	server.MsgDispatch(nil)
}
