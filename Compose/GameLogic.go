package Compose

import "fmt"

type IGL interface {
	MsgDispatch([]byte)
	GLMethod()
}

type GameLogic struct {
	*ServerMethod
}

func (gl *GameLogic) GLMethod() {
	//do some GL stuff
	fmt.Println("this is GLMethod")
}

func (gl *GameLogic) MsgDispatch(msg []byte) {
	//calling GS method
	switch {
	default:
		gl.GSMethod()
	}
}
