package Compose

import "fmt"

type ServerMethod struct {
	GSMethod func()
}

type GameServer struct {
	gl IGL
}

func (gs *GameServer) Start() {
	delgate := &ServerMethod{gs.GSMethod}
	gs.gl = &GameLogic{delgate}
}

func (gs *GameServer) GSMethod() {
	fmt.Println("this is GSMethod")
}

func (gs *GameServer) MsgDispatch(msg []byte) {
	switch {
	default:
		// do something
		gs.gl.GLMethod()
		// game msg
		gs.gl.MsgDispatch(msg)
	}
}
