package json

import (
	"encoding/json"
	"fmt"
)

// Object ...
type Object struct {
	Name string `json:"PlayerID,omitempty"`
	Idx  int    `json:"SeatID,omitempty"`
}

func Jsontest() {
	obj := Object{"Test", 5}
	data, _ := json.MarshalIndent(obj, "", "	")

	fmt.Printf("%s\n", data)

	var obj2 Object

	if err := json.Unmarshal(data, &obj2); err == nil {
		fmt.Println(obj2)
	}
}
